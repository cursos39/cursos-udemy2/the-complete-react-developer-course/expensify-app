import { get, push, ref, remove, update } from 'firebase/database'
import db from '../firebase/firebase'

// ADD_EXPENSE
const addExpense = (expense) => ({
  type: 'ADD_EXPENSE',
  expense
})

// ADD_EXPENSE_ASYNC
const startAddExpense = (expenseData = {}) => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid
    const {
      description = '',
      note = '',
      amount = 0,
      createdAt = 0
    } = expenseData
    const newExpense = { description, note, amount, createdAt }

    return push(ref(db, `users/${uid}/expenses`), newExpense)
      .then((newExpenseReference) => {
        dispatch(addExpense({
          id: newExpenseReference.key,
          ...newExpense
        }))
      })
  }
}

// REMOVE_EXPENSE
const removeExpense = ({ id: expenseId } = {}) => ({
  type: 'REMOVE_EXPENSE',
  expenseId
})

// EDIT_EXPENSE
const editExpense = (id, updates) => ({
  type: 'EDIT_EXPENSE',
  expenseId: id,
  updates
})

// SET_EXPENSES
export const setExpenses = expenses => ({
  type: 'SET_EXPENSES',
  expenses
})

export const startSetExpenses = () => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid
    return get(ref(db, `users/${uid}/expenses`))
      .then((expensesSnapshot) => {
        const expenses = []
        expensesSnapshot.forEach(childSnapshotExpense => {
          expenses.push({
            id: childSnapshotExpense.key,
            ...childSnapshotExpense.val()
          })
        })
        return dispatch(setExpenses(expenses))
      })
  }
}

export const startRemoveExpense = ({ id }) => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid
    return remove(ref(db, `users/${uid}/expenses/${id}`)).then(() => dispatch(removeExpense({ id })))
  }
}

export const startEditExpense = (id, updates) => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid
    delete updates.id
    return update(ref(db, `users/${uid}/expenses/${id}`), updates)
      .then(() => {
        return dispatch(editExpense(id, updates))
      })
      .catch(error => console.log(error))
  }
}

export { addExpense, removeExpense, editExpense, startAddExpense }
