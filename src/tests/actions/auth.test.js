import { login, logout } from '../../actions/auth'

test('login should return login action', () => {
  const action = login('12345')

  expect(action).toEqual({
    type: 'LOGIN',
    uid: '12345'
  })
})

test('logout should return login action', () => {
  const action = logout()

  expect(action).toEqual({
    type: 'LOGOUT'
  })
})
