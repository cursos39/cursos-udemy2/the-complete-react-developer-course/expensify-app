import React from 'react'
import { shallow } from 'enzyme'
import { ExpensesSummary } from '../../components/ExpensesSummary'
import expenses from '../fixtures/expenses'

test('should render correctly', () => {
  const wrapper = shallow(<ExpensesSummary expenses={expenses} amount={12}/>)

  expect(wrapper).toMatchSnapshot()
})

test('should render correctly one expense', () => {
  const wrapper = shallow(<ExpensesSummary expenses={[expenses[0]]} amount={5}/>)

  expect(wrapper).toMatchSnapshot()
})
