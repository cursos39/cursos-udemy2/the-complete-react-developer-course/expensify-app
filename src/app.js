import { onAuthStateChanged } from 'firebase/auth'
import 'normalize.css/normalize.css'
import numeral from 'numeral'
import 'numeral/locales/es-es'
import React from 'react'
import 'react-dates/lib/css/_datepicker.css'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { login, logout } from './actions/auth.js'
import { startSetExpenses } from './actions/expenses.js'
import { auth } from './firebase/firebase'
import AppRouter, { history } from './routers/AppRouter.js'
import configureStore from './store/configureStore.js'
import './styles/styles.scss'
import LoadingPage from './components/LoadingPage.js'

numeral.locale('es-es')

const store = configureStore()

const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
)

ReactDOM.render(<LoadingPage />, document.getElementById('app'))

const renderApp = ((applicationIsRendered) => {
  let hasRendered = applicationIsRendered
  const innerRenderApp = () => {
    if (!hasRendered) {
      ReactDOM.render(jsx, document.getElementById('app'))
      hasRendered = true
    }
  }
  return innerRenderApp
})(false)

onAuthStateChanged(auth, (user) => {
  if (user) {
    store.dispatch(login(user.uid))
    store.dispatch(startSetExpenses()).then(() => {
      renderApp()
      if (history.location.pathname === '/') {
        history.push('/dashboard')
      }
    })
  } else {
    store.dispatch(logout())
    renderApp()
    history.push('/')
  }
})
