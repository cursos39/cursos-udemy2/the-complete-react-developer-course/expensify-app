import React from 'react'
import { connect } from 'react-redux'
import { DateRangePicker } from 'react-dates'
import { generateSortBy, setTextFilter, setStartDate, setEndDate } from '../actions/filters'

export class ExpenseListFilters extends React.Component {
  state = {
    calendarFocused: null
  }

  onDatesChange = ({ startDate, endDate }) => {
    this.props.setStartDate(startDate)
    this.props.setEndDate(endDate)
  }

  onFocusChange = (calendarFocused) => {
    this.setState(() => ({ calendarFocused }))
  }

  onTextChange = (e) => {
    this.props.setTextFilter(e.target.value)
  }

  onSortChange = (e) => {
    this.props.generateSortBy(e.target.value)
  }

  render () {
    return (
    <div className="content-container">
      <div className='input-group'>
        <div className='input-group__item'>
          <input
            className='text-input'
            type="text"
            placeholder='Search for expenses'
            value={this.props.filters.text}
            onChange={this.onTextChange}
          />
        </div>
        <div className='input-group__item'>
          <select
            className='select'
            value={this.props.filters.sortBy}
            onChange={this.onSortChange}
          >
            <option value="date">Date</option>
            <option value="amount">Amount</option>
          </select>
        </div>
        <div className='input-group__item'>
          <DateRangePicker
            startDate={this.props.filters.startDate}
            startDateId="your_unique_start_date_id"
            endDate={this.props.filters.endDate}
            endDateId="your_unique_end_date_id"
            onDatesChange={this.onDatesChange}
            focusedInput={this.state.calendarFocused}
            onFocusChange={this.onFocusChange}
            numberOfMonths={1}
            isOutsideRange={() => false}
            showClearDates={true}
          />
        </div>
      </div>

    </div>)
  }
}

const mapStateToProps = state => ({
  filters: state.filters
})

const mapDispatchToProps = dispatch => ({
  setStartDate: startDate => dispatch(setStartDate(startDate)),
  setEndDate: endDate => dispatch(setEndDate(endDate)),
  setTextFilter: text => dispatch(setTextFilter(text)),
  generateSortBy: sortBy => dispatch(generateSortBy(sortBy))
})

export default connect(mapStateToProps, mapDispatchToProps)(ExpenseListFilters)
