[![Pipeline Master](https://gitlab.com/cursos-udemy2/the-complete-react-develpoer-course/expensify-app/badges/master/pipeline.svg)](https://gitlab.com/cursos-udemy2/the-complete-react-develpoer-course/expensify-app/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=expensify-app&metric=alert_status)](https://sonarcloud.io/dashboard?id=expensify-app)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=expensify-app&metric=coverage)](https://sonarcloud.io/dashboard?id=expensify-app)



# Expensify App

## Redux

* Redux [documentation][1]. 
* Uses [Redux-Thunk][11] for dispatching async actions to the store.
* Uses [Redux-Mock-Store][12] for testing.

## Enzyme

Enzyme [documentation][2].

## Jest

Jest [documentation][3].

## Git

### Git commands

* git init - Create a new git repo
* git status - View rhe changes to your project code
* git add - Add files to staging area
* git commit -m Create a new commit with files from staging area
* git log - View recent commits

## Express

Express [documentation][4].

## Heroku

We use [heroku][7] for hosting. The project is visible in this [url][8].

Instalation of the cli in MAC:

```shell
brew tap heroku/brew && brew install heroku
```

Next steps with the cli

```shell
heroku login
heroku create apecr-react-course-2-expensify
git push heroku master
heroku open
heroku logs
```

The [project dashboard][9].

## Firebase

We use Firebase to save expensify data in real time via Firebase Real Time Database.

* Firebase database [documentation][5].
* Data snapshot [documentation][10].
* Firebase authentication [documentation][13]. ANd the API [documentation][14].
* Firebase database rules [documentation][15], and database build [documentation][16].

Rules for the database:

```js
{
  "rules": {
    ".read": false,
    ".write": false,
    "users": {
			"$user_id": {
        ".read": "$user_id === auth.uid",
        ".write": "$user_id === auth.uid",
        "expenses": {
          "$expense_id": {
            ".validate": "newData.hasChildren(['description', 'note', 'createdAt', 'amount'])",
            "description": {
            	".validate": "newData.isString() && newData.val().length > 0"
            },
            "note": {
              ".validate": "newData.isString()"
            },
            "createdAt": {
              ".validate": "newData.isNumber()"
            },
            "amount": {
              ".validate": "newData.isNumber()"
            },
            "$other": {
              ".validate": false
            }
          }
        },
        "$other": {
          ".validate": false
        }
      }
    }
  }
}
```

## SASS

The page is styled with SCSS. The documentation is [here][17].

[1]: https://redux.js.org/
[2]: https://enzymejs.github.io/enzyme/
[3]: https://jestjs.io/docs/getting-started
[4]: https://expressjs.com/
[5]: https://firebase.google.com/docs/reference/js/database?hl=en
[6]: https://dashboard.heroku.com/apps/apecr-react-course-2-expensify
[7]: https://keroku.com/
[8]: https://apecr-react-course-2-expensify.herokuapp.com/
[9]: https://dashboard.heroku.com/apps/apecr-react-course-2-expensify
[10]: https://firebase.google.com/docs/reference/js/database.datasnapshot?hl=en
[11]: https://github.com/reduxjs/redux-thunk
[12]: https://github.com/reduxjs/redux-mock-store
[13]: https://firebase.google.com/docs?authuser=0
[14]: https://firebase.google.com/docs/reference/js/auth?hl=en&%3Bauthuser=0&authuser=0
[15]: https://firebase.google.com/docs/reference/security/database?authuser=0
[16]: https://firebase.google.com/docs/database/security?authuser=0
[17]: https://sass-lang.com/